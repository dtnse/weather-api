FORMAT: 1A
HOST: https://events.weather.mg/

# Weather Events Service

This service provides weather warnings generated in Wetter4 and Stormlab application via REST API endpoints.

## General information

This technical document was created for developers who are creating applications based on weather data provided by this API.
This document is using [API blueprint format]

## Authentication

To access weather-events-service the request has to be authorized with a valid
JWT OAuth 2.0 access token,  obtained from the DTN Authorization server
`https://auth.weather.mg/oauth/token`  with according client credentials. The
required scope is `events-api`.

The documentation about how to authenticate against DTN Weather API with
OAuth 2.0 can be found here: [Weather API documentation](https://bitbucket.org/dtnse/weather-api/src/master/authorization/Authentication.md)

## Endpoints

Weather Events Service serves two endpoints: Detailed Weather Events and Area Metadata

## Event level and derived parameters

For customers actively using legacy UWZ response fields: level and color.

The following table presents the equivalent severity, certainty, and urgency.

New clients should rely on combination of severity/certainty/urgency.

| Event level (text)     | Level (number) | Severity | Color     | Expected level | Warning type | Certainty*  | Urgency**          |
|------------------------|:--------------:|----------|-----------|----------------|--------------|-------------|--------------------|
| CLEAR                  | 0              | -        | -         | -              | -            | -           | -                  |
| NOTICE_FOREWARN_ORANGE | 1              | MINOR    | DARKGREEN | ORANGE         | FOREWARN     | POSSIBLE    | FUTURE             |
| NOTICE_FOREWARN_RED    | 2              | MINOR    | DARKGREEN | RED            | FOREWARN     | POSSIBLE    | FUTURE             |
| NOTICE_FOREWARN_VIOLET | 3              | MINOR    | DARKGREEN | VIOLET         | FOREWARN     | POSSIBLE    | FUTURE             |
| NOTICE_WARN_ORANGE     | 4              | MINOR    | DARKGREEN | ORANGE         | WARNING      | LIKELY      | EXPECTED/IMMEDIATE |
| NOTICE_WARN_RED        | 5              | MINOR    | DARKGREEN | RED            | WARNING      | LIKELY      | EXPECTED/IMMEDIATE |
| NOTICE_WARN_VIOLET     | 6              | MINOR    | DARKGREEN | VIOLET         | WARNING      | LIKELY      | EXPECTED/IMMEDIATE |
| ALERT_FOREWARN_ORANGE  | 7              | MODERATE | YELLOW    | ORANGE         | FOREWARN     | POSSIBLE    | FUTURE             |
| ALERT_FOREWARN_RED     | 8              | SEVERE   | YELLOW    | RED            | FOREWARN     | POSSIBLE    | FUTURE             |
| ALERT_FOREWARN_VIOLET  | 9              | EXTREME  | YELLOW    | VIOLET         | FOREWARN     | POSSIBLE    | FUTURE             |
| ALERT_WARN_ORANGE      | 10             | MODERATE | ORANGE    | ORANGE         | WARNING      | LIKELY      | EXPECTED/IMMEDIATE |
| ALERT_WARN_RED         | 11             | SEVERE   | RED       | RED            | WARNING      | LIKELY      | EXPECTED/IMMEDIATE |
| ALERT_WARN_VIOLET      | 12             | EXTREME  | VIOLET    | VIOLET         | WARNING      | LIKELY      | EXPECTED/IMMEDIATE |

*All ongoing or expired events (with "onset" in the past) have Certainty OBSERVED unless it has Warning Type FOREWARN of which it will have Certainty POSSIBLE.

**All expired events (with "expires" in the past) have Urgency PAST.

## Detailed Weather Events [/v1/weather-events/detailed]
## GET Detailed Weather Events [GET /v1/weather-events/detailed{?locatedAt,locatedWithin,countryCode,zipcode,fields,event,updated,effective,onset,expires,size,language,totalCount,page}]

+ Parameters

    + locatedAt: `-2.94,57.14` (string, required)

        Geographical point for point-based queries a geographical point (longitude, latitude).<br>
        Longitude should be within the range [-180.0; 180.0], and latitude should be within the range [-90.0; 90.0].<br>
        Only one of locatedAt, locatedWithin, countryCode or zipcode (with countryCode) must be provided.

    + locatedWithin: `[61.26,4.58],[58.67,9.92]` (string, required)

        Geographical bounding box as a pair of coordinates ([longitude, latitude],[longitude, latitude]) - [upper_left,lower_right].<br>
        Only one of locatedAt, locatedWithin, countryCode or zipcode (with countryCode) must be provided.

    + countryCode: `DE` (string, required)

        Two-letter country code. Used with zipcode to query for events in specific zipcode.<br>
        Used alone to query for events in whole country.<br>
        Only one of locatedAt, locatedWithin, countryCode or zipcode (with countryCode) must be provided.

    + zipcode: `80939` (string, optional)

        When set, response will return all events within the zipcode of the countryCode.<br>
        Area zipcode must be combined with countryCode.<br>
        Only one of locatedAt, locatedWithin, countryCode or zipcode with countryCode must be provided.

    + fields: `identifier,status` (string, required)

        Comma-delimited set of response field names to be returned in the response.<br>
        Possible values: identifier, status, level, language, areaId, areaDesc, zipcode, senderName, source, updated, effective, onset, expires, polygon, event, certainty, severity, urgency, category, altitude, ceiling, parameter, headline, description

    + event: `STORM,HEAVYRAIN,SNOWFALL,THUNDERSTORM,FREEZINGRAIN` (string, optional)

        Comma-delimited set of event types.<br>
        If provided, events will be filtered to ones having these event types.<br>
        If not provided, response will contain events with all possible types.<br>
        Possible values: FORESTFIRE, FROST, GROUNDFROST, HEAT, HEAVYRAIN, FREEZINGRAIN, SLIPPERINESS, SNOWFALL, STORM, THUNDERSTORM

    + updated: `2021-03-12T07:00:00Z` (string, required)

        When set, response will contain all events sent to the service or updated before this timestamp and expiring after this timestamp.<br>
        Must be in ISO8601 timestamp notation like: '2016-10-13T12:00:00Z' or '2011-12-03T10:15:30+01:00'.<br>
        Only one of updated, effective, onset must be provided.

    + effective: '2021-04-22T15:00:00.000Z' (string, required)

        When set, response will contain all events with active alert before this timestamp and expiring after this timestamp.<br>
        Must be in ISO8601 timestamp notation like: '2016-10-13T12:00:00Z' or '2011-12-03T10:15:30+01:00'.<br>
        Only one of updated, effective, onset must be provided.

    + onset: `2021-03-12T07:00:00Z` (string, required)

        When set, response will contain all events which weather event started before this timestamp and expiring after this timestamp.<br>
        Must be in ISO8601 timestamp notation like: '2016-10-13T12:00:00Z' or '2011-12-03T10:15:30+01:00'.<br>
        Only one of updated, effective, onset must be provided.

    + expires: `2021-03-12T07:00:00Z` (string, optional)

        If set, should be present in combination with "effective" field.<br>
        If set, response will contain all events which alert was active in any point of time between request's "effective" and "expires" timestamps - exclusive.<br>
        Must be in ISO8601 timestamp notation like: '2016-10-13T12:00:00Z' or '2011-12-03T10:15:30+01:00'.

    + size: `10` (integer, optional)

        Maximum number of events to be returned in one response.<br>
        If there are more events to be returned, new request for next page should be sent.<br>
        Exact link to this new request will be available in response under links > next field.
        Allowed range is between 0 and 500.
        Default value is 5.

    + language: `DE` (string, optional)

        Two-letter language code to set event text language.<br>
        Mandatory when requesting for fields: headline or description use any of [DE, DK, EN, ES, FI, FR, IT, LU, NL, NO, PT, SV].

    + totalCount: `true` (boolean, optional)

        If response should count total number of events matching search criteria (for pagination).<br>
        Default value is FALSE.

    + page: `2` (integer, optional)

        Requested page number (for pagination).<br>
        Default value is 1.

+ Response 200 (application/json)

    Returns detailed weather events.

    + Body

                {
                    "page": 1,
                    "total": 54,
                    "_links": {
                        "self": {
                            "href": "https://events.weather.mg/v1/weather-events/detailed?countryCode=DE&event=STORM,HEAVYRAIN,SNOWFALL&fields=certainty,severity,areaId&size=3&totalCount=true&updated=2021-03-27T10:00:00.000Z"
                        },
                        "next": {
                            "href": "https://events.weather.mg/v1/weather-events/detailed?countryCode=DE&event=STORM,HEAVYRAIN,SNOWFALL&fields=certainty,severity,areaId&page=2&size=3&totalCount=true&updated=2021-03-27T10:00:00.000Z"
                        }
                    },
                    "_embedded": {
                        "events": [
                            {
                                "geocode": {
                                    "areaId": "UWZDE72270"
                                },
                                "certainty": "OBSERVED",
                                "severity": "MODERATE"
                            },
                            {
                                "geocode": {
                                    "areaId": "UWZDE76534"
                                },
                                "certainty": "OBSERVED",
                                "severity": "MODERATE"
                            },
                            {
                                "geocode": {
                                    "areaId": "UWZDE76596"
                                },
                                "certainty": "OBSERVED",
                                "severity": "MODERATE"
                            }
                        ]
                    }
                }


## Area Meta Data [/v1/area-metadata]
## GET Area Meta Data [GET /v1/area-metadata{?zipcode,countryCode}]

+ Parameters

    + zipcode: `35423` (string, required)

        When set, response will return all events within the zipcode of the countryCode.<br>
        countryCode and Zipcode should be provided.

    + countryCode: `DE` (string, required)

        Two-letter country code.<br>
        countryCode and Zipcode should be provided.

+ Response 200 (application/json)

    Returns area metadata for the given zipCode and countryCode

    + Body

                {
                    "page": 1,
                    "_links": {
                        "self": {
                            "href": "https://events.weather.mg/v1/area-metadata?countryCode=DE&zipcode=74321"
                        }
                    },
                    "_embedded": {
                        "areaMetadata": {
                            "areaId": "UWZDE74321",
                            "areaName": "Bietigheim-Bissingen",
                            "zipcode": "74321",
                            "countryCode": "DE",
                            "altitude": 177,
                            "ceiling": 310,
                            "polygon": {
                                "type": "Polygon",
                                "coordinates": [
                                    [
                                        [
                                            9.15839,
                                            48.930585
                                        ],
                                        [
                                            9.151888,
                                            48.93066
                                        ],
                                        [
                                            9.158096,
                                            48.934666
                                        ],
                                        [
                                            9.15839,
                                            48.930585
                                        ]
                                    ]
                                ]
                            }
                        }
                    }
                }

