FORMAT: 1A

Weather API
=============================


# General information

This technical document was created for developers who are creating applications based on weather data provided by this API.
This document is using [API blueprint format](https://apiblueprint.org/documentation/).

### Authentication

To access point-forecast-service the request has to be authorised with a valid
JWT OAuth 2.0 access token,  obtained from the DTN Authorisation server
`https://auth.weather.mg/oauth/token`  with according client credentials. The
required scope is `point-forecast`, 'point-forecast-grid-based'.

The documentation about how to authenticate against DTN Weather API with
OAuth 2.0 can be found here: [Weather API documentation](https://bitbucket.org/dtnse/weather-api/src/master/authorization/Authentication.md)

### Backward compatibility

Every endpoint may add one or more response parameters in the future.
Clients should ignore unknown parameters, to avoid technical issues.


# Gridded Forecast Data

## Retrieve weather forecast [/gridded-forecast/hourly?locatedAt={locatedAt}&fields={fields}&validPeriod={validPeriod}&validFrom={validFrom}&validUntil={validUntil}&timeZone={timeZone}]

#### Example
`https://point-forecast.weather.mg/gridded-forecast/hourly?locatedAt=48.2047,11.4513&validPeriod=PT0S,PT1H,PT3H,PT12H,PT24H&validFrom=2020-06-24T11:00:00Z&validUntil=2020-06-29T11:00:00Z&fields=dewPointTemperatureInCelsius,maxAirTemperatureInFahrenheit,relativeGlobalRadiationInPercent,evaporationMakkinkInMillimeter&timeZone=UTC`

### Search for forecast data for multiple given locations [GET]

You can request forecast for geographical locations (long,lat) only using 'locatedAt' request parameter. This parameter is obligatory in the request.
If parameters validFrom and validUntil are missing then application sets validFrom as current time and validUntil as current time plus 1 day.
If parameter validFrom is not missing and validUntil is missing then application sets  validUntil as current time plus 1 day.
If parameter timeZone is missing forecasts will be returned in the UTC timezone.

*Meteorological hints*

The forecast parameters always have a time period associated, which is typically one hour and one moment.
Each weather parameter refers implicitly to one or another time period within a forecast.
For example air temperature is forecasted for one moment in time.
Whereas max air temperature is typically forecasted for a period of one hour.
There are other forecast periods possible, where parameters than are aggregated among e.g. 12 hours.
In the response JSON document the weather parameters will be associated with their according period.

All aggregated weather parameters (e.g. `averageWindSpeedInKnots`) are calculated at fixed hours as follows:
PT3H - at 00:00, 03:00, 06:00, 09:00, 12:00, 15:00, 18:00 and 21:00
PT6H - at 00:00, 06:00, 12:00 and 18:00
PT12H - at 06:00 and 18:00
PT24H - at 00:00

*Technical hints*

All timestamps and time periods/durations follow ISO8601 standard notation.
For example 'PT1H' refers to one hour and 'PT0S' refers to one moment.
For convenience reasons, a single location can be requested, but the response always returns a collection.
For multiple locations, its recommended to just use the point compression.
The forecasts in the response collection are "flat" to allow easy streaming and filtering on client side.
The forecasts in the response are ordered ascending by field 'validFrom'.

*Fields mapping*

Common names of filed which can be included in all periods: locatedAt,validFrom,validUntil,validPeriod,issuedAt,timeZone

Here are relationship between observation periods and fields appropriately:

- **PT0S** -    dewPointTemperatureInCelsius,airTemperatureNearGroundInCelsius,feelsLikeTemperatureInCelsius,visibilityProbabilityLessThan200MeterInPercent,
                weatherCode,weatherCodeTraditional,windSpeedInMilesPerHour,saturationDeficitInHectoPascal,
                effectiveCloudCoverInOcta,weatherSymbolCode,airTemperatureInKelvin,dewPointTemperatureInFahrenheit,
                uvIndexWithClouds,windSpeedInMeterPerSecond,effectiveCloudCoverInOkta,airTemperatureInCelsius,
                dewPointTemperatureInKelvin,effectiveCloudCoverInPercent,freezingRainProbabilityInPercent,clearSkyUVIndex,
                weatherSymbolCodeDay,airPressureAtSeaLevelInHectoPascal,weatherSymbolCodeNight,feelsLikeTemperatureInKelvin,
                humidexInCelsius,hailProbabilityInPercent,totalCloudCoverInOcta,visibilityProbabilityLessThan1000MeterInPercent,
                feelsLikeTemperatureInFahrenheit,condensationIndicator,windchillTemperatureInCelsius,windSpeedInKnots,
                windSpeedInKilometerPerHour,windSpeedInBeaufort,visibilityInMeter,windDirectionInDegree,
                convectivePrecipitationProbabilityInPercent,thunderstormProbabilityInPercent,snowfallProbabilityInPercent,precipitationType,
                totalCloudCoverInOkta,relativeHumidityInPercent,precipitationProbabilityInPercent,airTemperatureInFahrenheit
- **PT1H** -    sunshineDurationInMinutes,relativeGlobalRadiationInPercent,maxWindGustInKnots,hailProbabilityInPercent,
                maxWindGustInMeterPerSecond,maxPrecipitationAmountInMillimeter,globalRadiationInJoulePerSquareCentimeter,precipitationAmountInMillimeter,
                averageGlobalRadiationInWattPerSquareMeter,windGustProbabilityGreaterThan40KnotsInPercent,convectivePrecipitationProbabilityInPercent,
                thunderstormProbabilityInPercent,freshSnowfallInCentimeter,snowfallProbabilityInPercent,precipitationAmountInInch,
                relativeSunshineDurationInPercent,freezingRainProbabilityInPercent,maxWindGustInMilesPerHour,precipitationProbabilityInPercent,
                netRadiationInJoulePerCentimeterSquare,maxWindGustInKilometerPerHour
- **PT3H** -    maxAirTemperatureInFahrenheit,maxWindGustInKnots,averageWindSpeedInMeterPerSecond,weatherCode,
                weatherCodeTraditional,averageWindSpeedInKilometerPerHour,weatherSymbolCode,minAirTemperatureInFahrenheit,
                dominantWindDirectionInDegree,minFeelsLikeTemperatureInCelsius,averageWindSpeedInKnots,maxFeelsLikeTemperatureInFahrenheit,
                minFeelsLikeTemperatureInFahrenheit,maxUVIndexWithClouds,weatherSymbolCodeDay,minAirTemperatureInKelvin,
                weatherSymbolCodeNight,maxWindGustInMeterPerSecond,maxAirTemperatureInKelvin,averageAirPressureAtSeaLevelInHectoPascal,
                averageWindSpeedInBeaufort,averageRelativeHumidityInPercent,minFeelsLikeTemperatureInKelvin,averageWindSpeedInMilesPerHour,
                maxWindGustInMilesPerHour,minAirTemperatureInCelsius,maxFeelsLikeTemperatureInCelsius,precipitationProbabilityInPercent,
                maxFeelsLikeTemperatureInKelvin,maxWindGustInKilometerPerHour,maxAirTemperatureInCelsius,minVisibilityInMeter
- **PT6H** -    maxAirTemperatureInFahrenheit,averageWindSpeedInMeterPerSecond,averageWindSpeedInKilometerPerHour,minAirTemperatureInFahrenheit,
                dominantWindDirectionInDegree,minFeelsLikeTemperatureInCelsius,freshSnowfallInCentimeter,precipitationAmountInInch,
                averageWindSpeedInKnots,maxFeelsLikeTemperatureInFahrenheit,minFeelsLikeTemperatureInFahrenheit,maxUVIndexWithClouds,
                minAirTemperatureInKelvin,maxAirTemperatureInKelvin,averageAirPressureAtSeaLevelInHectoPascal,precipitationAmountInMillimeter,
                averageWindSpeedInBeaufort,averageRelativeHumidityInPercent,minFeelsLikeTemperatureInKelvin,precipitationDurationInMinutes,
                averageWindSpeedInMilesPerHour,minAirTemperatureInCelsius,maxFeelsLikeTemperatureInCelsius,precipitationProbabilityInPercent,
                maxFeelsLikeTemperatureInKelvin,maxAirTemperatureInCelsius,minVisibilityInMeter
- **PT12H** -   maxAirTemperatureInFahrenheit,averageWindSpeedInMeterPerSecond,precipitationProbabilityMoreThan20MillimeterInPercent,
                precipitationProbabilityMoreThan4_4MillimeterInPercent,effectiveCloudCoverInOcta,averageWindSpeedInKilometerPerHour,minAirTemperatureNearGroundInCelsius,
                minAirTemperatureInFahrenheit,dominantWindDirectionInDegree,minFeelsLikeTemperatureInCelsius,effectiveCloudCoverInOkta,
                precipitationAmountInInch,averageWindSpeedInKnots,maxFeelsLikeTemperatureInFahrenheit,minFeelsLikeTemperatureInFahrenheit,
                maxUVIndexWithClouds,effectiveCloudCoverInPercent,freezingRainProbabilityInPercent,mostSignificantWeatherCodeTraditional,
                precipitationProbabilityMoreThan1_4MillimeterInPercent,minAirTemperatureInKelvin,hailProbabilityInPercent,precipitationProbabilityMoreOrEqualThan0_1MillimeterInPercent,
                maxAirTemperatureInKelvin,averageAirPressureAtSeaLevelInHectoPascal,precipitationProbabilityMoreThan0_2MillimeterInPercent,
                precipitationAmountInMillimeter,averageWindSpeedInBeaufort,averageRelativeHumidityInPercent,minFeelsLikeTemperatureInKelvin,
                thunderstormProbabilityInPercent,snowfallProbabilityInPercent,averageWindSpeedInMilesPerHour,minAirTemperatureInCelsius,
                maxFeelsLikeTemperatureInCelsius,mostSignificantWeatherCode,groundFrostProbabilityInPercent,precipitationProbabilityInPercent,
                maxFeelsLikeTemperatureInKelvin,maxAirTemperatureInCelsius,minVisibilityInMeter
- **PT24H** -   evaporationMakkinkInMillimeter,averageWindSpeedInMeterPerSecond,maxPrecipitationAmountInMillimeter,averageAirPressureAtSeaLevelInHectoPascal,
                averageWindSpeedInBeaufort,averageWindSpeedInKilometerPerHour,averageRelativeHumidityInPercent,minAirTemperatureInFahrenheit,
                dominantWindDirectionInDegree,minFeelsLikeTemperatureInCelsius,minFeelsLikeTemperatureInKelvin,averageWindSpeedInKnots,
                maxFeelsLikeTemperatureInFahrenheit,minFeelsLikeTemperatureInFahrenheit,averageWindSpeedInMilesPerHour,maxUVIndexWithClouds,
                minAirTemperatureInCelsius,maxFeelsLikeTemperatureInCelsius,precipitationProbabilityInPercent,maxFeelsLikeTemperatureInKelvin,
                minAirTemperatureInKelvin,minVisibilityInMeter

+ The "weatherCode" parameter refers to: [this code table](FORECAST-WEATHER-API-WeatherCode.md).
+ The "weatherCodeTraditional" parameter refers to the WMO code table "4677".
+ The maxUVIndexWithClouds parameter is calculated based on clearSkyUVIndex and effectiveCloudCoverInOkta values
+ The minVisibilityInMeter parameters for PT3H, PT6H, PT12H and PT24H are calculated as a min value of the visibilityInMeter parameter from PT0S for the last 3, 6, 12 and 24 hours respectively,
    including marginal values. E.g for PT3H period 4 PT0S values are used to cover the full three hours (validUntil-3h,validUntil-2h,validUntil-1h and validUntil).
+ The min/max FeelsLikeTemperatureIn Celsius/Fahrenheit/Kelvin parameters for PT3H, PT6H, PT12H and PT24H are calculated as a min/max value of the feelsLikeTemperatureIn Celsius/Fahrenheit/Kelvin parameter from PT0S for the last 3, 6, 12 and 24 hours respectively, including marginal values. E.g for PT3H period 4 PT0S values are used to cover the full three hours (validUntil-3h,validUntil-2h,validUntil-1h and validUntil).


### Weather forecast for a given *latitude* and *longitude* [GET]

#### Example 1
'https://point-forecast.weather.mg/gridded-forecast/hourly?locatedAt=13.404954,52.520008&validPeriod=PT0S,PT1H,PT3H,PT6H,PT12H,PT24H&validFrom=2020-06-16T05:00:00Z&validUntil=2020-06-16T06:00:00Z&fields=dewPointTemperatureInCelsius,feelsLikeTemperatureInCelsius,maxAirTemperatureInFahrenheit,relativeGlobalRadiationInPercent,evaporationMakkinkInMillimeter,visibilityProbabilityLessThan200MeterInPercent,weatherCode,saturationDeficitInHectoPascal,precipitationProbabilityMoreThan4_4MillimeterInPercent&timeZone=Europe/Berlin'

+ Parameters
    + locatedAt: `13.404954,52.520008` (string, required) - longitude, latitude; can occur multiple times for multiple locations
    + fields: `dewPointTemperatureInCelsius,feelsLikeTemperatureInCelsius,maxAirTemperatureInFahrenheit,relativeGlobalRadiationInPercent,evaporationMakkinkInMillimeter,visibilityProbabilityLessThan200MeterInPercent,weatherCode,locatedAt,saturationDeficitInHectoPascal,precipitationProbabilityMoreThan4_4MillimeterInPercent,precipitationProbabilityInPercent` (string, required) - comma separated list of parameters to be contained inside response
    + validFrom: `2020-06-16T05:00:00Z` (string, optional) - ISO8601 timestamp notation, means incl. this timestamp; always provide a time offset, e.g. 'Z' for UTC; optional: a request may provide correct offset (so that server can convert to UTC).
    + validUntil: `2020-06-16T06:00:00Z` (string, optional) - ISO8601 timestamp notation, means incl. this timestamp; always provide a time offset, e.g. 'Z' for UTC; optional: a request may provide correct offset (so that server can convert to UTC).
    + validPeriod: `PT0S,PT1H,PT3H,PT6H,PT12H,PT24H` (string, required) - comma separated list of periods to be retrieved. PT1H refers to one hour forecast periods.
    + timeZone: `Europe/Berlin` (string, optional) - a time-zone ID or offset, such as `Europe/Paris`, `-01:00` etc.

+ Response 200 (application/json)

    + Headers
            Content-Type:application/json
            Last-Modified:Tue, 03 Jan 2017 09:29:41 GMT
            Cache-Control: max-age=90

    + Body

{
    "forecasts": [
        {
            "locatedAt": [
                13.404954,
                52.520008
            ],
            "validFrom": "2020-06-16T07:00:00+02:00",
            "validUntil": "2020-06-16T07:00:00+02:00",
            "validPeriod": "PT0S",
            "dewPointTemperatureInCelsius": 11.0,
            "feelsLikeTemperatureInCelsius": 17.3,
            "saturationDeficitInHectoPascal": 5.5,
            "visibilityProbabilityLessThan200MeterInPercent": 1,
            "weatherCode": 2
        },
        {
            "locatedAt": [
                13.404954,
                52.520008
            ],
            "validFrom": "2020-06-16T06:00:00+02:00",
            "validUntil": "2020-06-16T07:00:00+02:00",
            "validPeriod": "PT1H",
            "relativeGlobalRadiationInPercent": 96
        },
        {
            "locatedAt": [
                13.404954,
                52.520008
            ],
            "validFrom": "2020-06-16T08:00:00+02:00",
            "validUntil": "2020-06-16T08:00:00+02:00",
            "validPeriod": "PT0S",
            "dewPointTemperatureInCelsius": 11.1,
            "feelsLikeTemperatureInCelsius": 19.0,
            "saturationDeficitInHectoPascal": 7.2,
            "visibilityProbabilityLessThan200MeterInPercent": 1,
            "weatherCode": 1
        },
        {
            "locatedAt": [
                13.404954,
                52.520008
            ],
            "validFrom": "2020-06-16T07:00:00+02:00",
            "validUntil": "2020-06-16T08:00:00+02:00",
            "validPeriod": "PT1H",
            "relativeGlobalRadiationInPercent": 97
        },
        {
            "locatedAt": [
                13.404954,
                52.520008
            ],
            "validFrom": "2020-06-16T05:00:00+02:00",
            "validUntil": "2020-06-16T08:00:00+02:00",
            "validPeriod": "PT3H",
            "maxAirTemperatureInFahrenheit": 64.2,
            "weatherCode": 2
        },
        {
            "locatedAt": [
                13.404954,
                52.520008
            ],
            "validFrom": "2020-06-16T02:00:00+02:00",
            "validUntil": "2020-06-16T08:00:00+02:00",
            "validPeriod": "PT6H",
            "maxAirTemperatureInFahrenheit": 64.2
        },
        {
            "locatedAt": [
                13.404954,
                52.520008
            ],
            "validFrom": "2020-06-15T20:00:00+02:00",
            "validUntil": "2020-06-16T08:00:00+02:00",
            "validPeriod": "PT12H",
            "maxAirTemperatureInFahrenheit": 70.7,
            "precipitationProbabilityMoreThan4_4MillimeterInPercent": 0
        },
        {
            "locatedAt": [
                13.404954,
                52.520008
            ],
            "validFrom": "2020-06-15T08:00:00+02:00",
            "validUntil": "2020-06-16T08:00:00+02:00",
            "validPeriod": "PT24H",
            "evaporationMakkinkInMillimeter": 5.0
        }
    ]
}

# Example 2:
### Weather forecast for a given collection of *latitude* and *longitude* pairs, compressed by Point Compression Algorithm [GET]
'https://point-forecast.weather.mg/gridded-forecast/hourly?locatedAt=mr36-rnn3Cp8zq9zK&validPeriod=PT12H,PT24H&validFrom=2020-06-16T06:00:00Z&validUntil=2020-06-17T06:00:00Z&fields=precipitationProbabilityInPercent,minVisibilityInMeter&timeZone=Europe/Berlin'

+ Parameters
    + locatedAt: `mr36-rnn3Cp8zq9zK` (string, required) - compressed list of station locations, using Microsoft Point Compression Algorithm, is efficient for up to 400 locations
    + fields: `precipitationProbabilityInPercent,maxWindSpeedInKnots,stationTimeZoneName` (string, required) - comma separated list of parameters to be contained inside response
    + validFrom: `2020-06-16T06:00:00Z` (string, optional) - ISO8601 timestamp notation, means incl. this timestamp; always provide a time offset, e.g. 'Z' for UTC; optional: a request may provide correct offset (so that server can convert to UTC).
    + validUntil: `2020-06-17T06:00:00Z` (string, optional) - ISO8601 timestamp notation, means incl. this timestamp; always provide a time offset, e.g. 'Z' for UTC; optional: a request may provide correct offset (so that server can convert to UTC).
    + validPeriod: `PT12H,PT24H` (string, required) - comma separated list of periods to be retrieved. use iso8601 time duration notation. PT1H refers one hour forecast periods.
    + timeZone: `Europe/Berlin` (string, optional) - a time-zone ID or offset, such as `Europe/Paris`, `-01:00` etc.

+ Response 200 (application/json)

    + Headers
            Content-Type:application/json
            Last-Modified:Mon, 26 Dec 2016 14:28:05 GMT
            Cache-Control: max-age=90

    + Body

{
    "forecasts": [
        {
            "locatedAt": [
                19.94905,
                49.29903
            ],
            "validFrom": "2020-06-15T20:00:00+02:00",
            "validUntil": "2020-06-16T08:00:00+02:00",
            "validPeriod": "PT12H",
            "precipitationProbabilityInPercent": 81,
            "minVisibilityInMeter": 9830
        },
        {
            "locatedAt": [
                19.94905,
                49.29903
            ],
            "validFrom": "2020-06-15T08:00:00+02:00",
            "validUntil": "2020-06-16T08:00:00+02:00",
            "validPeriod": "PT24H",
            "precipitationProbabilityInPercent": 96,
            "minVisibilityInMeter": 4080
        },
        {
            "locatedAt": [
                19.94905,
                49.29903
            ],
            "validFrom": "2020-06-16T02:00:00+02:00",
            "validUntil": "2020-06-16T14:00:00+02:00",
            "validPeriod": "PT12H",
            "precipitationProbabilityInPercent": 93,
            "minVisibilityInMeter": 9830
        },
        {
            "locatedAt": [
                19.94905,
                49.29903
            ],
            "validFrom": "2020-06-15T14:00:00+02:00",
            "validUntil": "2020-06-16T14:00:00+02:00",
            "validPeriod": "PT24H",
            "precipitationProbabilityInPercent": 93,
            "minVisibilityInMeter": 9830
        },
        {
            "locatedAt": [
                19.94905,
                49.29903
            ],
            "validFrom": "2020-06-16T08:00:00+02:00",
            "validUntil": "2020-06-16T20:00:00+02:00",
            "validPeriod": "PT12H",
            "precipitationProbabilityInPercent": 93,
            "minVisibilityInMeter": 10410
        },
        {
            "locatedAt": [
                19.94905,
                49.29903
            ],
            "validFrom": "2020-06-15T20:00:00+02:00",
            "validUntil": "2020-06-16T20:00:00+02:00",
            "validPeriod": "PT24H",
            "precipitationProbabilityInPercent": 93,
            "minVisibilityInMeter": 9830
        },
        {
            "locatedAt": [
                19.94905,
                49.29903
            ],
            "validFrom": "2020-06-16T14:00:00+02:00",
            "validUntil": "2020-06-17T02:00:00+02:00",
            "validPeriod": "PT12H",
            "precipitationProbabilityInPercent": 79,
            "minVisibilityInMeter": 11480
        },
        {
            "locatedAt": [
                19.94905,
                49.29903
            ],
            "validFrom": "2020-06-16T02:00:00+02:00",
            "validUntil": "2020-06-17T02:00:00+02:00",
            "validPeriod": "PT24H",
            "precipitationProbabilityInPercent": 93,
            "minVisibilityInMeter": 9830
        },
        {
            "locatedAt": [
                19.94905,
                49.29903
            ],
            "validFrom": "2020-06-16T20:00:00+02:00",
            "validUntil": "2020-06-17T08:00:00+02:00",
            "validPeriod": "PT12H",
            "precipitationProbabilityInPercent": 34,
            "minVisibilityInMeter": 11030
        },
        {
            "locatedAt": [
                19.94905,
                49.29903
            ],
            "validFrom": "2020-06-16T08:00:00+02:00",
            "validUntil": "2020-06-17T08:00:00+02:00",
            "validPeriod": "PT24H",
            "precipitationProbabilityInPercent": 93,
            "minVisibilityInMeter": 10410
        },
        {
            "locatedAt": [
                19.94454,
                50.04969
            ],
            "validFrom": "2020-06-15T20:00:00+02:00",
            "validUntil": "2020-06-16T08:00:00+02:00",
            "validPeriod": "PT12H",
            "precipitationProbabilityInPercent": 36,
            "minVisibilityInMeter": 15380
        },
        {
            "locatedAt": [
                19.94454,
                50.04969
            ],
            "validFrom": "2020-06-15T08:00:00+02:00",
            "validUntil": "2020-06-16T08:00:00+02:00",
            "validPeriod": "PT24H",
            "precipitationProbabilityInPercent": 36,
            "minVisibilityInMeter": 14880
        },
        {
            "locatedAt": [
                19.94454,
                50.04969
            ],
            "validFrom": "2020-06-16T02:00:00+02:00",
            "validUntil": "2020-06-16T14:00:00+02:00",
            "validPeriod": "PT12H",
            "precipitationProbabilityInPercent": 48,
            "minVisibilityInMeter": 15380
        },
        {
            "locatedAt": [
                19.94454,
                50.04969
            ],
            "validFrom": "2020-06-15T14:00:00+02:00",
            "validUntil": "2020-06-16T14:00:00+02:00",
            "validPeriod": "PT24H",
            "precipitationProbabilityInPercent": 48,
            "minVisibilityInMeter": 15380
        },
        {
            "locatedAt": [
                19.94454,
                50.04969
            ],
            "validFrom": "2020-06-16T08:00:00+02:00",
            "validUntil": "2020-06-16T20:00:00+02:00",
            "validPeriod": "PT12H",
            "precipitationProbabilityInPercent": 48,
            "minVisibilityInMeter": 15900
        },
        {
            "locatedAt": [
                19.94454,
                50.04969
            ],
            "validFrom": "2020-06-15T20:00:00+02:00",
            "validUntil": "2020-06-16T20:00:00+02:00",
            "validPeriod": "PT24H",
            "precipitationProbabilityInPercent": 48,
            "minVisibilityInMeter": 15380
        },
        {
            "locatedAt": [
                19.94454,
                50.04969
            ],
            "validFrom": "2020-06-16T14:00:00+02:00",
            "validUntil": "2020-06-17T02:00:00+02:00",
            "validPeriod": "PT12H",
            "precipitationProbabilityInPercent": 23,
            "minVisibilityInMeter": 17450
        },
        {
            "locatedAt": [
                19.94454,
                50.04969
            ],
            "validFrom": "2020-06-16T02:00:00+02:00",
            "validUntil": "2020-06-17T02:00:00+02:00",
            "validPeriod": "PT24H",
            "precipitationProbabilityInPercent": 48,
            "minVisibilityInMeter": 15380
        },
        {
            "locatedAt": [
                19.94454,
                50.04969
            ],
            "validFrom": "2020-06-16T20:00:00+02:00",
            "validUntil": "2020-06-17T08:00:00+02:00",
            "validPeriod": "PT12H",
            "precipitationProbabilityInPercent": 18,
            "minVisibilityInMeter": 16460
        },
        {
            "locatedAt": [
                19.94454,
                50.04969
            ],
            "validFrom": "2020-06-16T08:00:00+02:00",
            "validUntil": "2020-06-17T08:00:00+02:00",
            "validPeriod": "PT24H",
            "precipitationProbabilityInPercent": 48,
            "minVisibilityInMeter": 15900
        }
    ]
}

# Data Structures

## Total Cloud Cover (number)

This symbolic digit shall embrace the total fraction of the celestial dome covered by clouds irrespective of their genus.
(WMO akronym 'N')

    Value | Meaning
    ------|---------
      0   |  0 (Sky completely clear)
      1   |  1 okta or less, but not zero
      2   |  2 oktas
      3   |  3 oktas
      4   |  4 oktas (Sky half cloudy)
      5   |  5 oktas
      6   |  6 oktas
      7   |  7 oktas
      8   |  8 oktas (Sky completely cloudy)
      9   |  Sky obscured from view (usually due to dense fog or heavy snow)
